In this project the original Micrium uc/OS-III kernel has been modified to have the following changes:

1. Earliest Deadline First (EDF) scheduling
2. Piority Ceiling Protocol (PCP) for resource management in mutex calls
3. API for Periodi task creation
